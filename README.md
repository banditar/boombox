# BoomBox

Sound recording and playback Java application

---
Useful for BeatBoxing wannabes.

---
**Description**\
Simple Java application.\
Done as an assignment _Java_ class at Charles University, for the III. semester of my Bachelor's Degree in Computer Science.

---
**Usage**\
When run: the terminal says what to do:
- Press and hold a key down on your keyboard
- Make a sound
- Release the button
    - [do the above steps as many times as you want]
- Hit _Esc_ to enter the "piano mode"
- Press the keys on which you have placed a sound.
    - [at this point sound is supposed to come out of your speaker]
- Play the piano with your own sounds
    - with Ctrl + [key], the sound on the _key_ will be looped for 5 times
- Press _Space_ to go back to sound recording
- Press _Esc_ to exit this cool application

---
Student:
- Jánosi József-Hunor

:8ball:

