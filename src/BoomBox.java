import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;

import javax.sound.sampled.LineUnavailableException;

/**
 * Records sounds to keys from the keyboard
 * and plays them back in 'piano mode',
 * where hitting the same key, plays the audio back.
 */
public class BoomBox {
    /**
     * Flag if ESC was pressed
     */
    private static volatile boolean exit = false;

    /**
     * Flag if program is in recording mode
     */
    private static boolean rec = true;

    /**
     * Tells if the file with the key[index] was created before
     */
    private static final boolean[] files =new boolean[256];

    /**
     * Makes a test recording for the first time.
     * I noticed, that the first recording is not played back nicely,
     * so I record something for the first time, to skip this wrong recording.
     */
    private static void firstRecord() {
        final SoundRecordingUtil recorder = new SoundRecordingUtil();
        Thread recordThread = new Thread(() -> {
            try {
                recorder.start();
            } catch (LineUnavailableException ex) {
                ex.printStackTrace();
                System.exit(-1);
            }
        });
        recordThread.start();
        recorder.stop();
    }

    public static void main(String[] args) {
        System.out.println("==Welcome to the BoomBox==\n");
        System.out.println("--Press and hold letter keys, on which you want to record a sound--");

        JFrame frame = new JFrame("BoomBox");
        Container contentPane = frame.getContentPane();
        JTextField textField = new JTextField();
        contentPane.add(textField, BorderLayout.NORTH);
        frame.pack();
        frame.setVisible(true);

        firstRecord();

        while (rec) {
            // recording mode
            System.out.println("--Hit ESC, to enter piano mode--\n");
            KeyListener listener = new KeyListener() {
                final boolean[] keys = new boolean[256];
                final SoundRecordingUtil[] recorder = new SoundRecordingUtil[256];

                @Override
                public void keyPressed(KeyEvent event) {
                    int c = event.getKeyChar();
                    if (c != 27) {
                        if (c >= 'a' && c <= 'z') {
                            if (!keys[c]) {
                                keys[c] = true;
                                files[c] = true;

                                // create a separate thread for recording
                                Thread recordThread = new Thread(() -> {
                                    try {
                                        System.out.println("Recording on '" + (char)c + "'");
                                        recorder[c] = new SoundRecordingUtil();
                                        recorder[c].start();
                                    } catch (LineUnavailableException ex) {
                                        ex.printStackTrace();
                                        System.exit(-1);
                                    }
                                });

                                recordThread.start();
                            }
                        }
                    } else {
                        exit = true;
                    }
                }

                @Override
                public void keyReleased(KeyEvent event) {
                    int c = event.getKeyChar();
                    if (c >= 'a' && c <= 'z') {
                        keys[c] = false;

                        try {
                            recorder[c].stop();
                            recorder[c].save(new File("Record_" + (char) c + ".wav"));
                            System.out.println("Record '" + (char)c + "' saved");
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    }
                }

                @Override
                public void keyTyped(KeyEvent event) {
                }
            };

            textField.addKeyListener(listener);

            while (!exit) Thread.onSpinWait();
            exit = false;
            System.out.println("\n--Exited from sound recording--\n----Piano Mode----\n");
            System.out.println("--Now hit the keys on which you recorded sounds, to play them back--");
            System.out.println("--If you press CTRL + 'the key', the sound will be looped for 5 times--");
            System.out.println("--Hit SPACE to go back to sound recording--");
            System.out.println("--Hit ESC to exit the BoomBox--\n");

            rec = false;

            textField.removeKeyListener(listener);

            // piano mode
            listener = new KeyListener() {
                @Override
                public void keyPressed(KeyEvent event) {
                    int mods = event.getModifiersEx();
                    int loops = 0;
                    int c = event.getKeyChar();

                    if (c == 32) {
                        // space
                        // go back to sound recording
                        rec = true;
                        exit = true;
                    } else {
                        if (KeyEvent.getModifiersExText(mods).equals("Ctrl")) {
                            c = c + 'a' - 1;
                            if (c >= 'a' && c <= 'z' && files[c]) {
                                loops = 5;
                                System.out.println("Looping '" + (char) c + "' for " + loops + " times");
                            }
                        }

                        if (c != 27) {
                            if (c >= 'a' && c <= 'z') {
                                if (files[c]) {
                                    int finalC = c;
                                    int finalLoops = loops;
                                    Thread playbackThread = new Thread(() -> {
                                        String audioFilePath = "Record_" + (char) finalC + ".wav";
                                        AudioPlayer player = new AudioPlayer();
                                        System.out.println("Playing '" + (char)finalC + "'");
                                        player.play(audioFilePath, finalLoops);
                                    });

                                    playbackThread.start();
                                }
                            }
                        } else {
                            exit = true;
                        }
                    }
                }

                @Override
                public void keyReleased(KeyEvent event) {
                }

                @Override
                public void keyTyped(KeyEvent event) {
                }
            };

            textField.addKeyListener(listener);

            while (!exit) Thread.onSpinWait();
            System.out.println("\n--Playing ended--\n");
            exit = false;

            textField.removeKeyListener(listener);
        }

        System.out.println("--BoomBox out--");
        System.exit(0);
    }
}